var program = require('commander');

var propinsiUrl  = 'http://nomor.net/_kodepos.php?_i=provinsi-kodepos&sby=000000';
var kabupatenUrl = 'http://nomor.net/_kodepos.php?_i=kota-kodepos&sby=000000';
var kecamatanUrl = 'http://nomor.net/_kodepos.php?_i=kecamatan-kodepos&sby=000000';
var kodeposUrl   = 'http://nomor.net/_kodepos.php?_i=desa-kodepos&sby=000000';

program
	.version('0.0.1')
	.description("Program kecil untuk scraping data dari http://nomor.net.\nCreated by Sugeng. S (me@sugeng.me)")

program
	.command('init')
  	.description('Membuat table yang dibutuhkan untuk proses scraping')
  	.action(function() {
  		var createTable = require('./libs/create-table');

  		return createTable.run();
  	})

program
	.command('propinsi')
  	.description('Scraping data propinsi dari http://nomor.net')
  	.action(function() {
  		var propinsi = require('./libs/propinsi');

  		return propinsi.run(propinsiUrl);
  	})

program
	.command('kabupaten')
  	.description('Scraping data kabupaten dari http://nomor.net')
  	.action(function() {
  		var kabupaten = require('./libs/kabupaten');

  		return kabupaten.run(kabupatenUrl);
  	})

program
	.command('kecamatan')
  	.description('Scraping data kecamatan dari http://nomor.net')
  	.action(function() {
  		var kecamatan = require('./libs/kecamatan');

  		return kecamatan.run(kecamatanUrl).then(function() { console.log('Proses scraping data kecamatan telah selesai.') });
  	})

program
	.command('kodepos')
  	.description('Scraping data kodepos/kelurahan/desa dari http://nomor.net')
  	.action(function() {
  		var kodepos = require('./libs/kodepos');

  		return kodepos.run(kodeposUrl).then(function() { console.log('Proses scraping data kodepos telah selesai.') });;
  	})

program.parse(process.argv);