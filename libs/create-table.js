var database = require('./database');
var Promise  = require('bluebird');

var tablePropinsi = "CREATE TABLE `propinsi` ("+
						"`id` INT(11) NOT NULL AUTO_INCREMENT,"+
						"`name` VARCHAR(100) NOT NULL DEFAULT '0',"+
						"`jumlah_kota` INT(11) NOT NULL DEFAULT '0',"+
						"`jumlah_kecamatan` INT(11) NOT NULL DEFAULT '0',"+
						"`jumlah_kelurahan` INT(11) NOT NULL DEFAULT '0',"+
						"`jumlah_pulau` INT(11) NOT NULL DEFAULT '0',"+
						"PRIMARY KEY (`id`)"+
					")"+
					"COLLATE='utf32_general_ci'"+
					"ENGINE=InnoDB";

var tableKabupaten = "CREATE TABLE `kabupaten` ("+
						"`id` INT(11) NOT NULL AUTO_INCREMENT,"+
						"`name` VARCHAR(255) NOT NULL DEFAULT '0',"+
						"`propinsi` VARCHAR(100) NOT NULL DEFAULT '0',"+
						"`jumlah_kecamatan` INT(11) NOT NULL DEFAULT '0',"+
						"`jumlah_kelurahan` INT(11) NOT NULL DEFAULT '0',"+
						"PRIMARY KEY (`id`)"+
					  ")"+
					  "COLLATE='utf32_general_ci'"+
					  "ENGINE=InnoDB";

var tableKecamatan = "CREATE TABLE `kecamatan` ("+
							"`id` INT(11) NOT NULL AUTO_INCREMENT,"+
							"`name` VARCHAR(255) NOT NULL DEFAULT '0',"+
							"`kabupaten` VARCHAR(255) NOT NULL DEFAULT '0',"+
							"`propinsi` VARCHAR(100) NOT NULL DEFAULT '0',"+
							"`jumlah_kelurahan` INT(11) NOT NULL DEFAULT '0',"+
							"PRIMARY KEY (`id`)"+
					  ")"+
					  "COLLATE='utf32_general_ci'"+
					  "ENGINE=InnoDB";

var tableKodepos = "CREATE TABLE `kodepos` ("+
						"`id` INT(11) NOT NULL AUTO_INCREMENT,"+
						"`kodepos` INT(11) NOT NULL,"+
						"`kelurahan` VARCHAR(100) NOT NULL,"+
						"`kecamatan` VARCHAR(100) NOT NULL,"+
						"`kabupaten` VARCHAR(100) NOT NULL,"+
						"`propinsi` VARCHAR(100) NOT NULL,"+
						"PRIMARY KEY (`id`)"+
					")"+
					"COLLATE='utf8_general_ci'"+
					"ENGINE=InnoDB"


var CreateTable = function() {};

CreateTable.prototype.run = function() {
	return new Promise(function(resolve, reject) {
		resolve(database.open());
	})
	.then(function() {
		console.log('Membuat table Propinsi.');
		return Promise.resolve(database.createTable(tablePropinsi));
	})
	.then(function() {
		console.log('Membuat table Kabupaten.');
		return Promise.resolve(database.createTable(tableKabupaten));
	})
	.then(function() {
		console.log('Membuat table Kecamatan.');
		return Promise.resolve(database.createTable(tableKecamatan));
	})
	.then(function() {
		console.log('Membuat table Kodepos.');
		return Promise.resolve(database.createTable(tableKodepos));
	})
	.finally(function(){
		database.close();
		return console.log('Proses pembuatan table telah selesai.');
	});
}

module.exports = new CreateTable();