var mysql 	= require('mysql');
var config 	= require('config');

var Database = function() {
	var connection;
}

Database.prototype.open = function() {
	connection = mysql.createConnection(config.get('database'));

	connection.connect(function(err) {
		if (err) {
			console.error('Tidak dapat koneksi dengan database: ' + err);
			process.exit();
		}
	});
}

Database.prototype.insert = function(table, values) {
	connection.query('INSERT INTO '+table+' SET ?', values, function(err, rows, fields) {
		if (err) {
			console.log(err);
		};
	});
}

Database.prototype.createTable = function(schema) {
	return connection.query(schema, function(err, result) {
		if (err) {
			console.log(err);
		};
	});
}

Database.prototype.close = function() {
	return connection.end();
}

module.exports = new Database();