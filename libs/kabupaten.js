var database = require('./database');
var request  = require('request');
var cheerio  = require('cheerio');
var Promise  = require('bluebird');

var open = function(url) {
	return new Promise(function(resolve, reject) {
		request(url, function(error, respons, html) {
			if (!error) {
				console.log('Processing URL ' + url);

				body = cheerio.load(html);
				resolve(body);
			}
		});
	});
}

var parse = function(body) {
	var data     = [];

	var $ = body;

	$('tr[bgcolor="#ccffff"]').each(function(){
		var kabupaten = {};

		cells = $(this).find('td');

		kabupaten.name 				= $(cells[2]).text() + ' ' + $(cells[3]).text();
		kabupaten.propinsi 			= $(cells[1]).text();
		kabupaten.jumlah_kecamatan  = $(cells[6]).text();
		kabupaten.jumlah_kelurahan 	= $(cells[7]).text().replace(/\./gi, '');

		data.push(kabupaten);
	});

	Promise.all(data)
		.each(function(kabupaten) {
			database.insert('kabupaten', kabupaten);
		});

	Promise.resolve(getNextPage(body));
}

var getNextPage = function(body) {
	var $ = body;

	var nextPage = $('.tpage').parent().find('b').next('a').attr('href');

	if (nextPage !== undefined) {
		return nextPage;
	} else {
		return false;
	}
}

var Kabupaten = function() {};

database.open();

Kabupaten.prototype.run = function(url) {
	var self = this;

	return open(url)
		.then(function(body) {
			return parse(body);
		})
		.then(function(nextPage) {
			if (nextPage !== undefined) {
				self.run(nextPage);
			} else {
				return Promise.resolve(false);
			}
		})
		.finally(function(){
			database.close();
			return console.log('Scraping data kabupaten telah selesai.');
		});
}

module.exports = new Kabupaten();