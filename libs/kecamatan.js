var database = require('./database');
var request  = require('request');
var cheerio  = require('cheerio');
var Promise  = require('bluebird');

var open = function(url) {
	return new Promise(function(resolve, reject) {
		request(url, function(error, respons, html) {
			if (!error) {
				console.log('Processing URL ' + url);

				body = cheerio.load(html);
				resolve(body);
			}
		});
	});
}

var parse = function(body) {
	var data     = [];

	var $ = body;

	$('tr[bgcolor="#ccffff"]').each(function(){
		var kecamatan = {};

		cells = $(this).find('td');

		kecamatan.name				= $(cells[4]).text()
		kecamatan.kabupaten			= $(cells[2]).text() + ' ' + $(cells[3]).text();
		kecamatan.propinsi 			= $(cells[1]).text();
		kecamatan.jumlah_kelurahan 	= $(cells[6]).text().replace(/\./gi, '');

		data.push(kecamatan);
	});

	Promise.all(data)
		.each(function(kecamatan) {
			database.insert('kecamatan', kecamatan);
		});

	return Promise.resolve(getNextPage(body));
}

var getNextPage = function(body) {
	var $ = body;

	$('br').remove();
	var nextPage = $('.tpage').parent().find('b').next('a').attr('href');

	if (nextPage !== undefined) {
		return nextPage;
	} else {
		return false;
	}
}

var Kecamatan = function() {};

database.open();

Kecamatan.prototype.run = function(url) {
	var self = this;

	return open(url)
		.then(function(body) {
			return parse(body);
		})
		.then(function(nextPage) {
			if (nextPage !== false) {
				return self.run(nextPage);
			} else {
				console.log('....');
				return database.close();
			}
		});
}

module.exports = new Kecamatan();