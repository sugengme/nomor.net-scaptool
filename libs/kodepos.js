var database = require('./database');
var request  = require('request');
var cheerio  = require('cheerio');
var Promise  = require('bluebird');

var open = function(url) {
	return new Promise(function(resolve, reject) {
		request(url, function(error, respons, html) {
			if (!error) {
				console.log('Processing URL ' + url);

				body = cheerio.load(html);
				resolve(body);
			}
		});
	});
}

var parse = function(body) {
	var data     = [];

	var $ = body;

	$('tr[bgcolor="#ccffff"]').each(function(){
		var kelurahan = {};

		cells = $(this).find('td');

		kelurahan.kodepos 		= $(cells[1]).text();
		kelurahan.kelurahan 	= $(cells[2]).text();
		kelurahan.kecamatan   	= $(cells[3]).text();
		kelurahan.kabupaten 	= $(cells[4]).text() + ' ' + $(cells[5]).text();
		kelurahan.propinsi    	= $(cells[6]).text();

		data.push(kelurahan);
	});

	Promise.all(data)
		.each(function(kelurahan) {
			database.insert('kodepos', kelurahan);
		});

	return Promise.resolve(getNextPage(body));
}

var getNextPage = function(body) {
	var $ = body;

	$('br').remove();
	var nextPage = $('.tpage').parent().find('b').next('a').attr('href');

	if (nextPage !== undefined) {
		return nextPage;
	} else {
		return false;
	}
}

var Kodepos = function() {};

database.open();

Kodepos.prototype.closeDb = function() {
	return database.close();
}

Kodepos.prototype.run = function(url) {
	var self = this;

	return open(url)
		.then(function(body) {
			return parse(body);
		})
		.then(function(nextPage) {
			if (nextPage !== false) {
				return self.run(nextPage);
			} else {
				console.log('....');
				return database.close();
			}
		});
}

module.exports = new Kodepos();