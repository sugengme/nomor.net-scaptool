var database = require('./database');
var request  = require('request');
var cheerio  = require('cheerio');
var Promise  = require('bluebird');

var open = function(url) {
	return new Promise(function(resolve, reject) {
		request(url, function(error, respons, html) {
			if (!error) {
				console.log('Processing URL ' + url);

				body = cheerio.load(html);
				resolve(body);
			}
		});
	});
}

var parse = function(body) {
	var data     = [];

	var $ = body;

	$('tr[bgcolor="#ccffff"]').each(function(){
		var propinsi = {};

		cells = $(this).find('td');

		propinsi.name 				= $(cells[1]).text();
		propinsi.jumlah_kota 		= $(cells[5]).text();
		propinsi.jumlah_kecamatan  	= $(cells[8]).text();
		propinsi.jumlah_kelurahan 	= $(cells[9]).text().replace(/\./gi, '');
		propinsi.jumlah_pulau    	= $(cells[10]).text().replace(/\./gi, '');

		data.push(propinsi);
	});

	Promise.all(data)
		.each(function(propinsi) {
			database.insert('propinsi', propinsi);
		});

	Promise.resolve(getNextPage(body));
}

var getNextPage = function(body) {
	var $ = body;

	var nextPage = $('.tpage').parent().find('b').next('a').attr('href');

	if (nextPage !== undefined) {
		return "http://nomor.net/" + nextPage;
	} else {
		return false;
	}
}

var Propinsi = function() {};

database.open();

Propinsi.prototype.run = function(url) {
	var self = this;

	return open(url)
		.then(function(body) {
			return parse(body);
		})
		.then(function(nextPage) {
			if (nextPage !== undefined) {
				self.run(nextPage);
			} else {
				return Promise.resolve(false);
			}
		})
		.finally(function(){
			database.close();
			return console.log('Scraping data propinsi telah selesai.');
		});
}

module.exports = new Propinsi();